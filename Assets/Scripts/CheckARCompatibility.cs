﻿using GoogleARCore;
using Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckARCompatibility : MonoBehaviour {

    [SerializeField]
    GameObject mARCORE;

    [SerializeField]
    GameObject mARTOOLKITX;

    [SerializeField]
    GameObject mDefaultScreen;

    [SerializeField]
    GameObject mDialog;

    [SerializeField]
    GameObject mSettings;

    [SerializeField]
    GameObject mARObject;
    [SerializeField]
    GameObject mARObject2;
    

    void Start (){
        Debug.Log("START");
#if UNITY_ANDROID
        if (mARCORE != null)
            mARCORE.SetActive(false);
        if (mSettings != null)
            mSettings.SetActive(false);
        if (mARTOOLKITX != null)
            mARTOOLKITX.SetActive(false);
        if (mDialog != null)
            mDialog.SetActive(false);
        if (mDefaultScreen != null)
            mDefaultScreen.SetActive(true);
        Debug.Log("START");
        StartCoroutine(CheckCompatibility());
#endif

#if UNITY_IOS
      mARTOOLKITX.SetActive(true);
#endif
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
                toastObject.Call("show");
            }));
        }
    }

    void startARCore()
    {
        if (mARCORE != null)
        {
            mDialog.SetActive(false);
            mDefaultScreen.SetActive(false);
            mARCORE.SetActive(true);
            mARObject2.transform.localScale = new Vector3(1, 1, 1);
            mARObject2.transform.Rotate(Vector3.right, 90f);

        }
    }

    public void startARToolkitX()
    {
        if (mARTOOLKITX != null)
        {
            
            mSettings.SetActive(true);
            mDialog.SetActive(false);
            mDefaultScreen.SetActive(false);
            mARTOOLKITX.SetActive(true);
            mARObject.GetComponent<AugmentedImageVisualizer>().enabled = false;
            mARObject2.GetComponent<AugmentedImageVisualizer>().enabled = false;
        }
    }

    public void installARCore()
    {
        mDialog.SetActive(false);
        Session.RequestApkInstallation(true);
    }

    private void askforARCoreInstallation()
    {
        mDialog.SetActive(true);
    }

    private IEnumerator CheckCompatibility()
    {
        AsyncTask<ApkAvailabilityStatus> checkTask = Session.CheckApkAvailability();
        CustomYieldInstruction customYield = checkTask.WaitForCompletion();
        yield return customYield;
        ApkAvailabilityStatus result = checkTask.Result;
        switch (result)
        {
            case ApkAvailabilityStatus.SupportedApkTooOld:
               // _ShowAndroidToastMessage("Supported apk too old");
                askforARCoreInstallation();
                break;
            case ApkAvailabilityStatus.SupportedInstalled:
                //_ShowAndroidToastMessage("Supported, installed START ARCORE");
                startARCore();
                break;
            case ApkAvailabilityStatus.SupportedNotInstalled:
               // _ShowAndroidToastMessage("Supported, not installed, requesting installation");
                askforARCoreInstallation();
                break;
            case ApkAvailabilityStatus.UnknownChecking:
                startARToolkitX();
                //_ShowAndroidToastMessage("Unknown Checking");
                break;
            case ApkAvailabilityStatus.UnknownError:
                startARToolkitX();
               // _ShowAndroidToastMessage("Unknown Error");
                break;
            case ApkAvailabilityStatus.UnknownTimedOut:
                startARToolkitX();
                //_ShowAndroidToastMessage("Unknown Timed out");
                break;
            case ApkAvailabilityStatus.UnsupportedDeviceNotCapable:
                startARToolkitX();
               // _ShowAndroidToastMessage("Unsupported Device Not Capable");
                Debug.Log("Unsupported device");
                break;
        }
    }
}
