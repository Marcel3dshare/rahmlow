﻿using System.Collections;
using UnityEngine;

public class Animation_Controller : MonoBehaviour {
    
    bool isNotPlaying;
    GameObject activeAnim;

    [SerializeField]
    GameObject[] interactiveObject;

    [SerializeField]
    GameObject[] animatedObjects;

    public GameObject trackedObject;

    Animation anim;
    AnimationClip animTake;

    void Start () 
    {
        isNotPlaying = true;
    }

    void Update ()
    {
        if( trackedObject.activeInHierarchy == false)
        {
            isNotPlaying = true;
            return;
        }
        
        if (isNotPlaying)
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    Ray ray = Camera.current.ScreenPointToRay(touch.position);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.gameObject.tag == "animation")
                        {
                            activeAnim = hit.collider.gameObject;
                            StartCoroutine(playanimation(activeAnim));
                        }
                    }
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray_mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray_mouse, out hit))
                {
                    if (hit.collider.gameObject.tag == "animation")
                    {
                        activeAnim = hit.collider.gameObject;
                        StartCoroutine(playanimation(activeAnim));
                    }
                }
            }
        }
    }

    void OnEnable()
    {
        if (activeAnim)
        {
            StartCoroutine(playanimation(activeAnim));
        }
    }

    IEnumerator playanimation(GameObject obj)
    {
        isNotPlaying = false;
        foreach (GameObject item in interactiveObject)
            item.SetActive(false);
     
        anim = obj.GetComponent<Animation>();

        animTake = anim.GetClip("Default Take");

        anim["Default Take"].speed = 1.0f;
        anim["Default Take"].time = 0.0f;

        anim.Play("Default Take");
        float waittime = animTake.length;

        yield return new WaitForSeconds(waittime+1.2f);

        anim["Default Take"].speed = -1.0f;
        anim["Default Take"].time = waittime;
        anim.Play("Default Take");
        
        yield return new WaitForSeconds(waittime + 0.3f);

        anim["Default Take"].speed = 1.0f;
        anim["Default Take"].time = 0.0f;
        
        isNotPlaying = true;
        foreach (GameObject item in interactiveObject)
            item.SetActive(true);
    } 
}