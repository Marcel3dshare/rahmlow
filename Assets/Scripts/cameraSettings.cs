﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraSettings: MonoBehaviour {
    
    public void openCameraSettings()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
                jo.Call("openCameraSettings");
            }
        }
    }
}